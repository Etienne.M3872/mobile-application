import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  AsyncStorage,
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import PropTypes from 'prop-types';
import * as Permissions from 'expo-permissions';
import { AntDesign } from '@expo/vector-icons';

import NotificationPanel from '../components/mainscreen/notification/NotificationPanel';
import ConfirmationModal from '../components/mainscreen/ConfirmationModal';
import MainCamera from '../components/mainscreen/MainCamera';
import Colors from '../constants/Colors';
import { NOTIFICATIONS } from '../constants/Variables';

const MainScreen = ({ navigation }) => {
  // STATES AND VARIABLES //
  const [hasCameraPermission, setHasCameraPermission] = useState(false);
  const [loaded, setLoaded] = useState(true);
  const [pickedImage, setPickedImage] = useState();
  const [modalVisible, setModalVisible] = useState(false);

  // CHECKS AND PREPARATION //
  // Check if signed in
  const fetchAuth = async () => {
    try {
      const value = await AsyncStorage.getItem('guestMode');
      if (value !== null) {
        return;
      }
    } catch (error) {
      // console.log('error fetching guest mode');
    }
    navigation.navigate('Sign in', {});
  };

  // Check and sets if permissions are enabled, will ask for permission if it was never asked before
  const checkOnMount = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    setHasCameraPermission(status === 'granted');
  };

  //  Once the component is mounted, check permissions, status, and set camera ratio
  useEffect(() => {
    fetchAuth();
    checkOnMount();
  }, [fetchAuth, checkOnMount]);

  // RENDERING //
  const notifs = () => (
    <NotificationPanel style={styles.notificationContainer} notifications={NOTIFICATIONS} />
  );

  const tabButton = () => (
    <TouchableWithoutFeedback onPress={() => { navigation.toggleDrawer(); }}>
      <View style={styles.icon}>
        <AntDesign name="menuunfold" size={15} />
      </View>
    </TouchableWithoutFeedback>
  );

  const renderNoPermissions = (text) => (
    <View style={styles.content}>
      {notifs()}
      <Text>{text}</Text>
      {tabButton()}
    </View>
  );

  // Final render if all goes right
  const renderBody = () => (
    <View style={styles.globalContainer}>
      <View style={styles.content}>
        <NavigationEvents onWillFocus={() => setLoaded(true)} onDidBlur={() => setLoaded(false)} />
        <ConfirmationModal
          pickedImage={pickedImage || ''}
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          navigation={navigation}
        />
        <View style={styles.cameraContainer}>
          {loaded && (
            <MainCamera setModalVisible={setModalVisible} setPickedImage={setPickedImage} />
          )}
        </View>
      </View>
      {tabButton()}
      {notifs()}
    </View>
  );

  let cameraScreenContent;
  if (hasCameraPermission === null) {
    cameraScreenContent = renderNoPermissions('Waiting for permissions');
  } else if (!hasCameraPermission) {
    cameraScreenContent = renderNoPermissions(
      'No access to camera. Enable camera permissions in our phone&apos;s settings',
    );
  } else {
    cameraScreenContent = renderBody();
  }

  return (
    <View style={styles.container}>
      {cameraScreenContent}
    </View>
  );
};

MainScreen.propTypes = {
  navigation: PropTypes.any.isRequired,
};

const styles = StyleSheet.create({
  cameraContainer: {
    alignItems: 'center',
    backgroundColor: Colors.black,
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    position: 'absolute',
    width: '100%',
  },
  container: {
    flex: 1,
  },
  content: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  globalContainer: {
    alignItems: 'center',
    flex: 1,
  },
  icon: {
    alignItems: 'center',
    backgroundColor: Colors.accent,
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10,
    elevation: 4,
    height: 35,
    justifyContent: 'center',
    left: 0,
    opacity: 0.8,
    overflow: 'hidden',
    position: 'absolute',
    top: '12%',
    width: 40,
  },
  notificationContainer: {
    borderColor: Colors.primary,
    borderWidth: 3,
    position: 'absolute',
    right: 0,
  },
});

export default MainScreen;
