import React from 'react';
import {
  View, Button, StyleSheet, AsyncStorage, Alert,
} from 'react-native';
import PropTypes from 'prop-types';

const SignInScreen = (props) => {
  const storeGuest = async (isOn) => {
    try {
      await AsyncStorage.setItem('guestMode', isOn);
    } catch (error) {
      Alert.alert(
        'Error',
        'Error occured saving guest mode',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      );
    }
  };

  return (
    <View style={styles.content}>
      <View style={styles.buttons}>
        <Button
          title="Login with Facebook"
          disabled
          onPress={() => {
            Alert.alert(
              'Unimplemented',
              'Accounts are not enabled as of right now',
              [{ text: 'OK' }],
            );
            storeGuest('false');
          }}
        />
        <Button
          title="Login with Google"
          disabled
          onPress={() => {
            Alert.alert(
              'Unimplemented',
              'Accounts are not enabled as of right now',
              [{ text: 'OK' }],
            );
            storeGuest('false');
          }}
        />
        <Button
          title="Continue as guest"
          onPress={() => {
            storeGuest('true');
            props.navigation.navigate('Fruit Scanner');
          }}
        />
      </View>
    </View>
  );
};

SignInScreen.navigationOptions = () => ({
  headerTitle: 'Login',
});

SignInScreen.propTypes = {
  navigation: PropTypes.any.isRequired,
};

const styles = StyleSheet.create({
  buttons: {
    height: 150,
    justifyContent: 'space-between',
    marginTop: 20,
    width: 200,
  },
  content: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

export default SignInScreen;
