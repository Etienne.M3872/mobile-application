import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Platform,
  Alert,
  ActivityIndicator,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import * as ImageManipulator from 'expo-image-manipulator';
import * as ImagePicker from 'expo-image-picker';

import Icon from '../DynamicIcon';
import Colors from '../../constants/Colors';

const MainCamera = ({ setModalVisible, setPickedImage }) => {
  const [type, setType] = useState(Camera.Constants.Type.back);
  const [flashMode, setFlashMode] = useState(Camera.Constants.FlashMode.off);
  const [ratio, setRatio] = useState();
  const [cameraHeight, setCameraHeight] = useState();
  const [isSnapping, setIsSnapping] = useState(false);
  const [isBarcodeScan, setIsBarcodeScan] = useState(false);
  const [cameraZoom, setCameraZoom] = useState(0);
  const [extraToggled, setExtraToggled] = useState(false);

  let camera;

  // Check the barcode data
  const processBarCode = (data) => {
    if (isBarcodeScan) {
      console.log(data);
    }
  };

  const snap = () => {
    if (camera) {
      setIsSnapping(true);
      camera.takePictureAsync({ quality: 0.8 }).then((photo) => {
        ImageManipulator.manipulateAsync(photo.uri, [{
          crop: {
            originX: photo.width * 0.175,
            originY: photo.height * 0.20,
            width: photo.width * 0.65,
            height: photo.width * 0.65,
          },
        }]).then((croppedPhoto) => {
          setPickedImage(croppedPhoto.uri);
          setIsSnapping(false);
          setModalVisible(true);
        });
      });
    }
  };

  const selectPicture = async () => {
    const { status } = await Permissions.getAsync(Permissions.CAMERA);
    if (status !== 'granted') {
      Alert.alert(
        'Permissions needed',
        'Please enable camera roll permissions in order to upload a picture from your gallery. You can go in your phone\'s settings to enable them.',
      );
      return;
    }
    ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      quality: 0.55,
      aspect: [1, 1],
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
    })
      .then(({ cancelled, uri }) => {
        if (!cancelled) {
          setPickedImage(uri);
          setModalVisible(true);
        }
      });
  };

  const prepareRatios = async () => {
    if (Platform.OS === 'android' && camera) {
      const ratios = await camera.getSupportedRatiosAsync();
      // Get the maximum supported ratio for the current device
      // Usually the last element of "ratios" is the maximum supported ratio
      const selectedRatio = ratios[ratios.length - 1];

      setRatio(selectedRatio);
      const screenHeight = Dimensions.get('window').height;
      const screenWidth = Dimensions.get('window').width;
      const [hR, wR] = selectedRatio.split(':');
      /* eslint-disable no-mixed-operators */
      let height = (screenWidth / parseInt(wR, 10)) * parseInt(hR, 10) / screenHeight * 100;
      if (height > 100) {
        height = 100;
      }
      setCameraHeight(`${height}%`);
    }
  };

  const changeFlash = () => (
    flashMode === Camera.Constants.FlashMode.off ? (
      setFlashMode(Camera.Constants.FlashMode.on)
    ) : (flashMode === Camera.Constants.FlashMode.on ? (
      setFlashMode(Camera.Constants.FlashMode.auto)
    ) : (
      setFlashMode(Camera.Constants.FlashMode.off)
    ))
  );

  const flashIconName = () => (
    flashMode === Camera.Constants.FlashMode.off ? (
      'flash-off'
    ) : (flashMode === Camera.Constants.FlashMode.on ? (
      'flash-on'
    ) : (
      'flash-auto'
    ))
  );

  return (
    <Camera
      ref={(ref) => { camera = ref; }}
      onCameraReady={prepareRatios}
      ratio={ratio}
      type={type}
      flashMode={flashMode}
      onBarCodeScanned={(data) => processBarCode(data)}
      zoom={cameraZoom}
      style={{ ...styles.camera, maxHeight: cameraHeight }}
    >
      {/* Picture frame */}
      <View style={styles.viewBox} />
      {/* Extra buttons */}
      <View style={styles.extraContainer}>
        <Icon
          onPressHandler={() => { setExtraToggled(!extraToggled); }}
          name={extraToggled ? 'chevron-up' : 'chevron-down'}
          type="Feather"
        />
        {/* Barcode toggle button */}
        {extraToggled && (
          <Icon
            onPressHandler={() => { setIsBarcodeScan(!isBarcodeScan); }}
            name="barcode-scan"
            type="MaterialCommunityIcons"
          >
            {!isBarcodeScan && (
              <Ionicons name="md-close" size={40} style={styles.txtIcon} />
            )}
          </Icon>
        )}
        {/* Image upload button */}
        {extraToggled && (
          <Icon
            onPressHandler={async () => { await selectPicture(); }}
            name="upload"
            type="MaterialCommunityIcons"
          />
        )}
      </View>
      {/* Footer buttons */}
      <View style={styles.bottomButtons}>
        {/* Camera Flip button */}
        <Icon
          touchableStyle={styles.btn}
          onPressHandler={() => {
            setType(
              type === Camera.Constants.Type.back
                ? Camera.Constants.Type.front
                : Camera.Constants.Type.back,
            );
          }}
          name="camera-switch"
          type="MaterialCommunityIcons"
        />
        {/* FlashMode */}
        <Icon
          touchableStyle={styles.btn}
          onPressHandler={changeFlash}
          name={flashIconName()}
          type="MaterialIcons"
          size={25}
        />
        {/* Take picture button */}
        {!isSnapping ? (
          <Icon
            touchableStyle={styles.btn}
            onPressHandler={async () => { await snap(); }}
            name="camera-iris"
            type="MaterialCommunityIcons"
            iconStyle={styles.icon}
          />
        ) : (
          <View style={styles.btn}>
            <ActivityIndicator style={styles.icon} size="large" color="#000000" />
          </View>
        )}
        {/* Zoom out button */}
        <Icon
          touchableStyle={styles.btn}
          onPressHandler={() => { setCameraZoom(cameraZoom - 0.1 < 0 ? 0 : cameraZoom - 0.1); }}
          name="zoom-out"
          type="Feather"
        />
        {/* Zoom in button */}
        <Icon
          touchableStyle={styles.btn}
          onPressHandler={() => { setCameraZoom(cameraZoom + 0.1 > 1 ? 1 : cameraZoom + 0.1); }}
          name="zoom-in"
          type="Feather"
        />
      </View>
    </Camera>
  );
};

MainCamera.propTypes = {
  setModalVisible: PropTypes.func.isRequired,
  setPickedImage: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  bottomButtons: {
    backgroundColor: Colors.transparent,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  btn: {
    alignItems: 'center',
    alignSelf: 'flex-end',
    marginBottom: 35,
    opacity: 0.8,
  },
  camera: {
    flex: 1,
    width: '100%',
  },
  extraContainer: {
    alignItems: 'flex-end',
    flex: 1,
    flexDirection: 'column-reverse',
    justifyContent: 'flex-start',
    right: 20,
  },
  icon: {
    alignSelf: 'flex-end',
    backgroundColor: Colors.accent,
    borderRadius: 35,
    elevation: 4,
    height: 70,
    opacity: 0.8,
    overflow: 'hidden',
    textAlign: 'center',
    textAlignVertical: 'center',
    width: 70,
  },
  txtIcon: {
    alignSelf: 'center',
    color: Colors.accent,
    fontSize: 40,
    marginBottom: 10,
    position: 'absolute',
  },
  viewBox: {
    alignSelf: 'center',
    borderColor: Colors.white,
    borderRadius: 10,
    borderWidth: 3,
    height: Dimensions.get('window').width * 0.65,
    justifyContent: 'center',
    position: 'relative',
    top: '20%',
    width: Dimensions.get('window').width * 0.65,
  },
});

export default MainCamera;
