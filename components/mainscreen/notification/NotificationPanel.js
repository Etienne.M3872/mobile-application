import React from 'react';
import {
  StyleSheet,
  FlatList,
} from 'react-native';
import PropTypes from 'prop-types';
import NotificationItem from './NotificationItem';

const NotificationPanel = ({ notifications }) => {
  // Higher severity/oldest first. 1 is the highest priority
  const sortedNotifications = notifications.sort((a, b) => {
    if (a.severity < b.severity) return -1;
    if (a.sentDate < b.sentDate && a.severity === b.severity) return -1;
    return 0;
  });

  // Render flatlist  of notificationitems
  return (
    <FlatList
      style={styles.notificationList}
      data={sortedNotifications}
      keyExtractor={(n) => (`${n.id}`)}
      renderItem={({ item }) => (
        <NotificationItem
          id={item.id}
          title={item.title}
          type={item.type}
          description={item.body}
          isDeletable={item.isDeletable}
        />
      )}
    />
  );
};

NotificationPanel.propTypes = {
  notifications: PropTypes.array.isRequired,
};

const styles = StyleSheet.create({
  notificationList: {
    maxHeight: 500,
    position: 'absolute',
    right: 0,
    top: 50,
    width: 60,
  },
});

export default NotificationPanel;
