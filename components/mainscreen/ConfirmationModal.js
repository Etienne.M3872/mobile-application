import React, { useState } from 'react';
import {
  Alert, View, Button, Text, Image, Modal, StyleSheet, Dimensions, ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';

import axios from 'axios';
import Colors from '../../constants/Colors';

const ConfirmationModal = ({
  pickedImage,
  modalVisible,
  setModalVisible,
  navigation,
}) => {
  const [isUploading, setIsUploading] = useState(false);

  const uploadImageAsync = async (uri) => {
    const uriParts = uri.split('.');
    const fileType = uriParts[uriParts.length - 1];
    const formData = new FormData();
    formData.append('photo', {
      uri,
      name: `photo.${fileType}`,
      type: `image/${fileType}`,
    });

    return axios.post('http://206.167.36.206/api/image', formData, {
      accept: 'application/json',
      'Accept-Language': 'en-US,en;q=0.8',
      'Content-Type': `multipart/form-data; boundary=${formData._boundary}`,
      timeout: 15000,
      maxContentLength: 100 * 1024 * 1024,
    })
      .then((res) => {
        setModalVisible(!modalVisible);
        navigation.navigate('PictureDetails', {
          image: pickedImage,
          results: res.data,
        });
      })
      .catch((e) => {
        console.log(e);
        Alert.alert('Error',
          'Error fetching response from the server. \n Make sure you are using a photo under 5mb and make sure you are connected to the internet.',
          [{ text: 'OK' }]);
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Modal
      animationType="slide"
      transparent
      visible={modalVisible}
    >
      <View style={styles.modal}>
        <View style={styles.modalContent}>
          <View>
            <View style={styles.imagePreview}>
              {!pickedImage ? (
                <View style={styles.imageEmpty} />
              ) : (
                <Image style={styles.image} source={{ uri: pickedImage }} />
              )}
            </View>
            <Text style={styles.modalText}>
              {!isUploading ? 'Do you want to send this picture?' : 'Analyzing picture...'}
            </Text>
            {!isUploading
              ? (
                <View style={styles.modalActions}>
                  <View style={styles.modalAction}>
                    <Button
                      title="Back"
                      color={Colors.primary}
                      onPress={() => setModalVisible(!modalVisible)}
                    />
                  </View>
                  <View style={styles.modalAction}>
                    <Button
                      title="Send"
                      color={Colors.primary}
                      onPress={() => {
                        setIsUploading(true);
                        uploadImageAsync(pickedImage);
                      }}
                    />
                  </View>
                </View>
              )
              : <ActivityIndicator size="large" />}
          </View>
        </View>
      </View>
    </Modal>
  );
};

ConfirmationModal.propTypes = {
  pickedImage: PropTypes.string.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  setModalVisible: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
  image: {
    height: '100%',
    width: '100%',
  },
  imageEmpty: {
    backgroundColor: Colors.transparent,
    height: '100%',
    width: '100%',
  },
  imagePreview: {
    alignItems: 'center',
    borderColor: Colors.gray,
    borderWidth: 1,
    height: Dimensions.get('window').width * 0.60,
    justifyContent: 'center',
    marginBottom: 10,
    width: Dimensions.get('window').width * 0.60,
  },
  modal: {
    alignItems: 'center',
    top: Dimensions.get('window').height * 0.15,
  },
  modalAction: {
    width: 100,
  },
  modalActions: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
    paddingHorizontal: 15,
  },
  modalContent: {
    alignItems: 'center',
    backgroundColor: Colors.white,
    borderRadius: 15,
    height: Dimensions.get('window').height * 0.60,
    justifyContent: 'center',
    width: Dimensions.get('window').width * 0.90,
  },
  modalText: {
    fontSize: 15,
    marginVertical: 15,
    textAlign: 'center',
  },
});

export default ConfirmationModal;
