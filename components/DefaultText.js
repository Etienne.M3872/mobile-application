import React from 'react';
import { Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const DefaultText = ({ children }) => <Text style={styles.text}>{children}</Text>;

DefaultText.propTypes = {
  children: PropTypes.any,
};

DefaultText.defaultProps = {
  children: '',
};

const styles = StyleSheet.create({
  text: {
    fontFamily: 'open-sans',
  },
});

export default DefaultText;
