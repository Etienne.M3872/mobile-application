import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import {
  MaterialCommunityIcons,
  MaterialIcons,
  Feather,
} from '@expo/vector-icons';
import PropTypes from 'prop-types';

import Colors from '../constants/Colors';

const DynamicIcon = ({
  touchableStyle,
  onPressHandler,
  name,
  type,
  size,
  iconStyle,
  children,
}) => {
  let Icon;
  // Add more icon types if needed, for now only these are used in the app
  switch (type) {
    case 'MaterialCommunityIcons':
      Icon = MaterialCommunityIcons;
      break;
    case 'MaterialIcons':
      Icon = MaterialIcons;
      break;
    case 'Feather':
      Icon = Feather;
      break;
    default:
      break;
  }

  return (
    <TouchableOpacity style={touchableStyle} onPress={onPressHandler}>
      <Icon name={name} type={type} size={size || 40} style={iconStyle || styles.txtIcon} />
      {children}
    </TouchableOpacity>
  );
};

DynamicIcon.propTypes = {
  onPressHandler: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  txtIcon: {
    color: Colors.accent,
    fontSize: 40,
    marginBottom: 10,
  },
});

export default DynamicIcon;
