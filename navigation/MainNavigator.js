import React from 'react';
import {
  Platform, View,
  Text, SafeAreaView,
  StyleSheet,
} from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerNavigatorItems } from 'react-navigation-drawer';

import TabBarIcon from '../components/TabBarIcon';
import SignInScreen from '../screens/SignInScreen';
import MainScreen from '../screens/MainScreen';
import PictureDetailsScreen from '../screens/PictureDetailsScreen';
import SettingsScreen from '../screens/SettingsScreen';
import Colors from '../constants/Colors';

const customDrawerNavigation = (props) => (
  <View style={{ flex: 1, paddingTop: 20 }}>
    <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
      <View style={styles.labelContainer}>
        <Text style={styles.labelHeader}>
          IFMAP - LRIMa
        </Text>
        <Text style={styles.labelBody}>
          Jihene Rezgui, Thomas Trépanier, David Génois, Hervé Claden and Amasten Ameziani
        </Text>
      </View>
      {/* eslint-disable react/jsx-props-no-spreading */}
      <DrawerNavigatorItems {...props} />
    </SafeAreaView>
  </View>
);

const defaultStackNavOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === 'android' ? Colors.primary : '',
  },
  headerTitleStyle: {
    fontFamily: 'open-sans-bold',
  },
  headerBackTitleStyle: {
    fontFamily: 'open-sans',
  },
  headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary,
  headerTitle: 'Default Title',
};

const AuthStack = createStackNavigator({
  SignIn: {
    screen: SignInScreen,
    navigationOptions: {
      headerTitle: 'Welcome!',
      guestMode: 'Stay in guest mode',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavOptions,
  navigationOptions: {
    drawerIcon: () => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-log-in' : 'md-log-in'}
      />
    ),
  },
});

const MainStack = createStackNavigator({
  Main: {
    screen: MainScreen,
    navigationOptions: {
      header: null,
    },
  },
  PictureDetails: {
    screen: PictureDetailsScreen,
    navigationOptions: {
      headerTitle: 'Analysis Results',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavOptions,
  navigationOptions: {
    drawerIcon: () => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-qr-scanner' : 'md-qr-scanner'}
      />
    ),
  },
});

const SettingsStack = createStackNavigator({
  Settings: {
    screen: SettingsScreen,
    navigationOptions: {
      headerTitle: 'Settings',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavOptions,
  navigationOptions: {
    drawerIcon: () => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-settings' : 'md-settings'}
      />
    ),
  },
});

const MainDrawer = createDrawerNavigator({
  'Fruit Scanner': MainStack,
  'Sign in': {
    screen: AuthStack,
    navigationOptions: {
      drawerLockMode: 'locked-closed',
    },
  },
  'App Settings': SettingsStack,
}, {
  defaultNavigationOptions: defaultStackNavOptions,
  drawerType: 'slide',
  minSwipeDistance: 500,
  title: 'test',
  contentComponent: customDrawerNavigation,
});

const App = createSwitchNavigator({
  App: MainDrawer,
}, {
  defaultNavigationOptions: defaultStackNavOptions,
});

const styles = StyleSheet.create({
  labelBody: {
    color: Colors.gray,
    fontFamily: 'poppins-regular',
    fontSize: 14,
  },
  labelContainer: {
    alignContent: 'center',
    alignItems: 'center',
    borderBottomColor: Colors.gray,
    borderBottomWidth: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
  },
  labelHeader: {
    fontFamily: 'poppins-regular',
    fontSize: 28,
  },
});

export default createAppContainer(App);
